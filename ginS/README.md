#Gin Default Server

This is API experiment for Gin.

```go
package main

import (
	"bitbucket.org/sfdug/gin"
	"bitbucket.org/sfdug/gin/ginS"
)

func main() {
	ginS.GET("/", func(c *gin.Context) { c.String("Hello World") })
	ginS.Run()
}
```
